#include <stdio.h>
int main()
{
    int a,b,*ptr1,*ptr2,sum,diff,prod;
    float quo;
    printf("Enter two integers : \n");
    scanf("%d%d",&a,&b);
    ptr1=&a;
    ptr2=&b;
    sum= *ptr1 + *ptr2;
    diff=*ptr1-*ptr2;
    prod=(*ptr1)*(*ptr2);
    quo=((float)(*ptr1))/((float)(*ptr2));
    printf("sum = %d\ndifference = %d\nproduct = %d\nquotient = %f",sum,diff,prod,quo);
    return 0;
}
