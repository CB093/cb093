#include <stdio.h>
#include <stdlib.h>
void sort(int n,int v[n])
{
    for(int i=0;i<n-1;i++)
    {
        int iMin=i;
        for(int j=i+1;j<n;j++)
        {
            if(v[j]<v[iMin])
            iMin=j;
        }
        int aux=v[i];
        v[i]=v[iMin];
        v[iMin]=aux;
         
    }
}
int main()
{
    /*The following program first creates a file and stores n integers into it and then reads data from the file
      and sorts it and stores it into another file*/
    int num,n,numbers[100],i;
    char ch;
    FILE *fp1;
    fp1 = fopen("numbers.txt","w");
     
    if(fp1 == NULL)
    {
        printf("Error");   
        exit(0);             
    }
    printf("Enter the number of numbers to be stored in the file : \n");
    scanf("%d",&n);
    printf("Enter the numbers:\n");
    for(i=0;i<n;i++)
    {
        printf("Enter num: ");
        scanf("%d",&num);
        fprintf(fp1,"%d ",num);
    }
    fclose(fp1);
    FILE *fp2;
    fp2=fopen("numbers.txt","r");
    i=0;
    while(fscanf(fp2, "%d%c", &num,&ch)>0)
    {
        printf("%d ",num);
        numbers[i]=num;
        i++;    
    }
    sort(n,numbers);
    fclose(fp2);
    FILE *fp3;
    fp3=fopen("snumbers.txt","w");
    if(fp3 == NULL)
    {
        printf("Error");   
        exit(0);             
    }
    printf("\nArray added to file : ");
    for(i=0;i<n;i++)
    {
        printf("%d ",numbers[i]);
        fprintf(fp3,"%d ",numbers[i]);
    }
    fclose(fp3);
    return 0;
    
}

