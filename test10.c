#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
int main () {
	FILE *fp;
	int num;
	fp=fopen("INPUT.txt","w");
	if(fp==NULL) {
		printf("Error opening file");
		exit(1);
	}
	printf("Enter num: ");
    scanf("%d",&num);
	fprintf(fp,"%d",num);
    
    int n;
    fclose(fp);
    FILE *fp2;
    fp2 = fopen("INPUT.txt","r");
    fscanf(fp2,"%d",&n);
	printf("Value of n=%d",n);
    fclose(fp2);
    return 0;
}