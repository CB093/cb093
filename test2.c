#include <stdio.h>
void input(int* h,int* m)
{
	printf("Enter time in hours and minutes\n");
	scanf("%d %d",h,m);
}	
int conversion(int h,int m)
{
	return(h*60 + m);
}
	
void display(int m)
{
	printf("The time in minutes is %d",m);
}

int main()
{
	int h,m;
	input(&h,&m);
	m=conversion(h,m);
	display(m);
    return 0;
}
