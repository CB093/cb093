#include <stdio.h>

int main()
{
    int small,large,n,i,t,arr[25];
    printf("Enter size of array (Max 25)\n");
    scanf("%d",&n);
    printf("Enter elements of the array\n");
    for(i=0;i<n;i++)
    scanf("%d",&arr[i]);
    small=0;
    large=0;
    printf("Array before swapping :\n ");
    for(i=0;i<n;i++)
    printf("%d ",arr[i]);
    for(i=1;i<n;i++)
    {
        if(arr[i]<arr[small])
        small=i;
        if(arr[i]>arr[large])
        large=i;
    }
    t=arr[small];
    arr[small]=arr[large];
    arr[large]=t;
    printf("\nArray after swapping :\n ");
    for(i=0;i<n;i++)
    printf("%d ",arr[i]);
    return 0;
}
