#include <stdio.h>
float input()
{
	float r;
	printf("Enter radius \n");
	scanf("%f",&r);
	return r;
}
float Area(float r)
{
	return (3.142*r*r);
}
void display(float area)
{
	printf("Area of the circle = %f",area);
}
int main()
{
	float r=input();
	display(Area(r));
	return 0;
