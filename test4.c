#include<stdio.h>
int input()
{
	int n;
	printf("Enter a number\n");
	scanf("%d",&n);
	return n;
}	
int sum_of_digits(int n)
{
	int sum=0;
	while(n>0)
	{
		sum+=n%10;
		n/=10;
	}
    return sum;
}
void display(int n,int sum)
{
	printf("Sum of digits of %d is %d",n,sum);
}
int main()
{
	int n = input();
	int sum = sum_of_digits(n);
	display(n,sum);
	return 0;
}		
