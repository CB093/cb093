#include <stdio.h>
int main()
{
    int arr[25],n,i,sum=0,mean;
    printf("Enter the number of elements in the array : \n");
    scanf("%d",&n);
    printf("Enter the elements of the array : \n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
        sum+=arr[i];
    }
    mean = sum/n;
    printf("Elements greater than the mean : \n");
    for(i=0;i<n;i++)
    {
        if(arr[i]>mean)
        printf("%d ",arr[i]);
    }
    return 0;
}
