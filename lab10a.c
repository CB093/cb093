int main()
{
    int arr[25],n,i,sum=0;
    float avg;
    printf("Enter the number of elements in the array : \n");
    scanf("%d",&n);
    printf("Enter the elements of the array : \n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
        sum+=arr[i];
    }
    avg = sum*(1.0)/n;
    printf("Average of the numbers : %f",avg);
    return 0;
}
